## 补全程序，实现从内存 1000:0000 处开始执行指令

```asm
assume cs: code
stack segment
    db 16 dup (0)
stack ends
code segment

    start: mov ax,stack
        mov ss,ax
        mov sp,16
        mov ax,______     ;1000H

        push ax 
        mov ax,______     ;0
        push ax 
        retf
code ends
end start
```
## ret 和 retf
- ret 指令用栈中的数据，修改为的内容，从而实现近转；
- retf指令用栈中的数据，修改CS 和 IP的内容，从而实现远转移

- 用汇编语法来解释ret 和retf 指令，则：
- CPU执行 ret 指令时，相当于进行： 
```asm 
pop IP
```

- CPU执行retf 指令时，相当于进行：
```asm
pop IP 
pop CS
```
- 下面的程序中，ret 指令执行后，(P)=0，CS:P指向代码段的第一条指令。
```asm
assume cs:code
stack segment 
    db 16 dup (0)
stack ends

code segment
    mov ax,4c00h
    int 21h
    start: mov ax,stack    
    mov ss,ax 
    mov sp,16
    mov ax,0 
    push ax 
    mov bx,0 
    ret
code ends
end start


- 下面的程序中，retf 指令执行后，CS:IP 指向代码段的第一系指令。
```asm
assume cs:code
stack segment
    db 16 dup (0)
stack ends

code segment
    mov ax,4c00h 
    int 21h
    start: mov ax,stack
    mov ss,ax 
    mov sp,16
    mov ax,0 
    push cs 
    push ax 
    mov bx,0 
    retf
code ends
end start

## 转移地址在寄存器中的 call 指令
- 指令格式：call 16位 reg
<br>
- 用汇编语法来解释此种格式的 call 指令，CPU执行“call 16 位reg”时，相当于进行:
  - push IP
  - jmp 16位 reg
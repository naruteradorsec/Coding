## 下面的程序执行后，ax中的数值为多少？
<br>
```asm
内存地址   机器码              汇编指今
1000:0   b8 00 00           mov ax, O
1000:3   9A 09 00 00 10     call far ptr s
1000:8   40                 inc ax
1000:9   58                 s:pop ax
                              add ax,ax
                              pop bx
                              add ax,bx
```
- ax = 1010H
## mul指令
- 指令，mul是乘法指令，使用mu 做乘法的时候，注意以下两点:
- 两个相乘的数:两个相乘的数，要么都是8位，要么都是16位。如果是8位，一个默认放在 AL 中，另一个放在8位 reg 或内存字节单元中；如果是16位，一个默认在 AX 中，另一个放在16位 reg 或内存字单元中
- 计算结果:如果是8位乘法，结果默认放在 AX 中；如果是16位乘法，结果高位默认在 DX 中存放，低位在 AX 中放
<br>
- 格式如下：
mul reg
mul 内存单元

- 计算 100 * 10:
  - 100 和10小于 255，可以做8位乘法，程序如下:
```asm
mov al,100
mov b1,10 
mul bl
```
- 结果:(ax)=1000(03E8H）

- 计算 100 * 10000:
  - 100 小于 255，可10000大于 255， 所以必须做 16位乘法，程序如下:
```asm
mov ax, 100
mov bx, 10000 
mul bx
```
结果:(ax)=4240H,(dx)=000FH        (F4240H=1000000)